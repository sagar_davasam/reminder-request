package com.bytes.reminderrequest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

public class SMSStatusReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.

        if (intent.getAction().equals(SendSMSAsync.SENT)) {
            if (getResultCode() != FragmentActivity.RESULT_OK) {
                Toast.makeText(context, "Reminder Sending failed to: " + intent.getStringExtra(SendSMSAsync.CONTACT_DISPLAY_NAME), Toast.LENGTH_LONG).show();
            }
        } else if (intent.getAction().equals(SendSMSAsync.DELIVERED)) {
            if (getResultCode() == FragmentActivity.RESULT_OK) {
                Toast.makeText(context, "Reminder Delivered to: " + intent.getStringExtra(SendSMSAsync.CONTACT_DISPLAY_NAME), Toast.LENGTH_LONG).show();
            }
        }
    }
}
