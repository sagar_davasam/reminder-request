package com.bytes.reminderrequest;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class SMSReceiverService extends Service {
    private static final String ACTION_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private SMSReceiver smsReceiver;

    public SMSReceiverService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        smsReceiver = new SMSReceiver();
        registerReceiver(smsReceiver, new IntentFilter(ACTION_SMS_RECEIVED));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(smsReceiver);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
