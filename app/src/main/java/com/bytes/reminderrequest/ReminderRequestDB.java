package com.bytes.reminderrequest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by sagar.davasam on 11/12/15.
 */
public class ReminderRequestDB {

    private SQLiteDatabase sqLiteDatabase;

    private DatabaseHelper databaseHelper;

    private static final String DATABASE_NAME = "reminder_request_db";

    private static final int DATABASE_VERSION = 1;

    private static final int READABLE_DB = 0;

    private static final int WRITABLE_DB = 1;

    private static final String TAG = "ReminderRequestDB";

    ArrayList<Reminder> futureRemindersList;

    public ReminderRequestDB(final Context context){
        databaseHelper = new DatabaseHelper(context);
    }

    public void openDB(int getType){

        switch (getType){
            case READABLE_DB:
                sqLiteDatabase = databaseHelper.getReadableDatabase();
                break;
            case WRITABLE_DB:
                sqLiteDatabase = databaseHelper.getWritableDatabase();
                break;
        }

    }

    public void closeDB(){

        databaseHelper.close();

    }

    public long insertData(Reminder reminder) {
        //SQLiteDatabase db = databaseHelper.getWritableDatabase();
        openDB(WRITABLE_DB);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ReminderUtils.TIMESTAMP_FORMAT);

        long epoch = new Date().getTime();
        try {
            epoch = simpleDateFormat.parse(reminder.getReminderTimeStamp()).getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put(databaseHelper.COLUMN_CONTACT, reminder.getReminderContact());
        contentValues.put(databaseHelper.COLUMN_IS_RECEIVED, reminder.getbReceived());
        contentValues.put(databaseHelper.COLUMN_REMINDER_TIMESTAMP, epoch);
        contentValues.put(databaseHelper.COLUMN_REMINDER_MESSAGE, reminder.getReminderMessage().replaceAll("'", "\'"));
        contentValues.put(databaseHelper.COLUMN_RECORD_TIMESTAMP, simpleDateFormat.format(new Date()));
        contentValues.put(databaseHelper.COLUMN_IS_READ, reminder.getbRead());

        long returnValue = sqLiteDatabase.insert(databaseHelper.TABLE_REMINDER_REQUEST, null, contentValues);

        closeDB();

        return returnValue;
    }

    public Cursor getAllRecords() {

        openDB(READABLE_DB);
        String selectQuery = "SELECT " + databaseHelper.COLUMN_ID + "," + databaseHelper.COLUMN_CONTACT + "," + databaseHelper.COLUMN_REMINDER_MESSAGE + ","
                + databaseHelper.COLUMN_REMINDER_TIMESTAMP + "," + databaseHelper.COLUMN_IS_READ + " FROM " + databaseHelper.TABLE_REMINDER_REQUEST
                + " WHERE " + databaseHelper.COLUMN_IS_RECEIVED + "= " + ReminderUtils.REMINDER_RECEIVED
                + " ORDER BY " + databaseHelper.COLUMN_ID + " DESC";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        return cursor;
    }

    public Cursor getSentItems() {
        openDB(READABLE_DB);
        String selectQuery = "SELECT " + databaseHelper.COLUMN_ID + "," + databaseHelper.COLUMN_CONTACT + "," + databaseHelper.COLUMN_REMINDER_MESSAGE + ","
                + databaseHelper.COLUMN_REMINDER_TIMESTAMP + " FROM " + databaseHelper.TABLE_REMINDER_REQUEST
                + " WHERE " + databaseHelper.COLUMN_IS_RECEIVED + "= " + ReminderUtils.REMINDER_SENT
                + " ORDER BY " + databaseHelper.COLUMN_ID + " DESC";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        return cursor;
    }

    public void updateRecord(Reminder receivedReminder, Reminder editedReminder) {
        try {
            openDB(WRITABLE_DB);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ReminderUtils.TIMESTAMP_FORMAT);
            long receivedReminderEpoch = simpleDateFormat.parse(receivedReminder.getReminderTimeStamp()).getTime();
            long editedReminderEpoch = simpleDateFormat.parse(editedReminder.getReminderTimeStamp()).getTime();

            String updateQuery = "INSERT OR REPLACE INTO " + databaseHelper.TABLE_REMINDER_REQUEST
                    + " (" + databaseHelper.COLUMN_ID + ", "
                    + databaseHelper.COLUMN_CONTACT + ", "
                    + databaseHelper.COLUMN_REMINDER_TIMESTAMP + ", "
                    + databaseHelper.COLUMN_REMINDER_MESSAGE + ", "
                    + databaseHelper.COLUMN_RECORD_TIMESTAMP + ", "
                    + databaseHelper.COLUMN_IS_READ + ", "
                    + databaseHelper.COLUMN_IS_RECEIVED + ") " +
                    "VALUES ((SELECT " + databaseHelper.COLUMN_ID + " FROM " + databaseHelper.TABLE_REMINDER_REQUEST +
                    " WHERE " + databaseHelper.COLUMN_CONTACT + " = '" + receivedReminder.getReminderContact()
                    + "' AND " + databaseHelper.COLUMN_REMINDER_TIMESTAMP + " = '" + receivedReminderEpoch
                    + "' AND " + databaseHelper.COLUMN_REMINDER_MESSAGE + " = '" + receivedReminder.getReminderMessage().replaceAll("'", "\'") + "'), '"
                    + editedReminder.getReminderContact() + "', '"
                    + editedReminderEpoch + "', '"
                    + editedReminder.getReminderMessage().replaceAll("'", "\'") + "', '"
                    + new SimpleDateFormat(ReminderUtils.TIMESTAMP_FORMAT).format(new Date()) + "', "
                    + editedReminder.getbRead() + ", "
                    + editedReminder.getbReceived() + ");";
            sqLiteDatabase.execSQL(updateQuery);
            closeDB();
        } catch (Exception ex) {
            Log.e(TAG, "Update record exception caught: ", ex);
        }

    }

    public void deleteRecord(Reminder reminder) {

        openDB(WRITABLE_DB);
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ReminderUtils.TIMESTAMP_FORMAT);
            long epoch = simpleDateFormat.parse(reminder.getReminderTimeStamp()).getTime();

            String deleteQuery = "DELETE FROM " + databaseHelper.TABLE_REMINDER_REQUEST + " WHERE "
                    + databaseHelper.COLUMN_REMINDER_MESSAGE + " = '" + reminder.getReminderMessage() + "' AND "
                    + databaseHelper.COLUMN_REMINDER_TIMESTAMP + " = '" + epoch + "' AND "
                    + databaseHelper.COLUMN_CONTACT + " = '" + reminder.getReminderContact() + "'";
            //+ databaseHelper.COLUMN_IS_RECEIVED + " = " +reminder.getbReceived();

            sqLiteDatabase.execSQL(deleteQuery);
            closeDB();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void deleteUnreadRecord(Reminder reminder) {

        openDB(WRITABLE_DB);
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ReminderUtils.TIMESTAMP_FORMAT);
            long epoch = simpleDateFormat.parse(reminder.getReminderTimeStamp()).getTime();

            String deleteQuery = "DELETE FROM " + databaseHelper.TABLE_REMINDER_REQUEST + " WHERE "
                    + databaseHelper.COLUMN_REMINDER_MESSAGE + " = '" + reminder.getReminderMessage() + "' AND "
                    + databaseHelper.COLUMN_REMINDER_TIMESTAMP + " = '" + epoch + "' AND "
                    + databaseHelper.COLUMN_CONTACT + " = '" + reminder.getReminderContact() + "'"
                    + " AND " + databaseHelper.COLUMN_IS_READ + "=  " + ReminderUtils.REMINDER_MESSAGE_UNREAD;
            //+ databaseHelper.COLUMN_IS_RECEIVED + " = " +reminder.getbReceived();

            sqLiteDatabase.execSQL(deleteQuery);
            closeDB();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public Cursor getFutureRecords(){
        //SQLiteDatabase db = databaseHelper.getReadableDatabase();
        openDB(READABLE_DB);

        long currentDateTimeInEpoch = new Date().getTime();
        String selectQuery = "SELECT "+ databaseHelper.COLUMN_ID+ "," +databaseHelper.COLUMN_CONTACT+","+databaseHelper.COLUMN_REMINDER_MESSAGE +","
                                + databaseHelper.COLUMN_REMINDER_TIMESTAMP +" FROM "+ databaseHelper.TABLE_REMINDER_REQUEST
                + " WHERE " + databaseHelper.COLUMN_REMINDER_TIMESTAMP + " > " + currentDateTimeInEpoch
                + " AND " + databaseHelper.COLUMN_IS_RECEIVED + "=  '" + ReminderUtils.REMINDER_RECEIVED
                + "' AND " + databaseHelper.COLUMN_IS_READ + "=  '" + ReminderUtils.REMINDER_MESSAGE_READ
                + "' ORDER BY " + databaseHelper.COLUMN_REMINDER_TIMESTAMP + " ASC";

        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);


        if(cursor != null || cursor.getCount()>0){
            cursor.moveToFirst();
        }

        closeDB();

        return cursor;
    }

    static class DatabaseHelper extends SQLiteOpenHelper{

        public static final String TABLE_REMINDER_REQUEST = "reminder_request_table";

        public static final String COLUMN_ID = "_id";

        public static final String COLUMN_CONTACT = "contact";

        public static final String COLUMN_REMINDER_TIMESTAMP = "reminder_timestamp";

        public static final String COLUMN_RECORD_TIMESTAMP = "record_timestamp";

        public static final String COLUMN_REMINDER_MESSAGE = "reminder_message";

        public static final String COLUMN_IS_RECEIVED = "is_received";

        public static final String COLUMN_IS_READ = "is_read";

        private static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS "+ TABLE_REMINDER_REQUEST + "("
                                                        + COLUMN_ID + " integer primary key autoincrement, "
                                                        + COLUMN_CONTACT +" text not null, "
                                                        + COLUMN_REMINDER_TIMESTAMP + " numeric not null, "
                                                        + COLUMN_REMINDER_MESSAGE + " text not null, "
                                                        + COLUMN_RECORD_TIMESTAMP + " numeric not null, "
                                                        + COLUMN_IS_READ + " numeric not null, "
                                                        + COLUMN_IS_RECEIVED + " numeric not null);";

        private static final String DATABASE_DELETE = "DROP TABLE IF EXISTS " + TABLE_REMINDER_REQUEST;

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

            sqLiteDatabase.execSQL(DATABASE_DELETE);
            onCreate(sqLiteDatabase);

        }
    }

}
