package com.bytes.reminderrequest;

import android.content.Context;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by sagar.davasam on 11/29/15.
 */
public class CustomGestureListener extends GestureDetector.SimpleOnGestureListener {

    private RelativeLayout frontLayout;
    private RelativeLayout backLayout;
    private Animation inFromRight, inFromLeft, outToRight, outToLeft;

    public CustomGestureListener(Context context, View view){
        backLayout = (RelativeLayout)view.findViewById(R.id.layout_back);
        frontLayout = (RelativeLayout)view.findViewById(R.id.layout_front);
        inFromRight = AnimationUtils.loadAnimation(context, R.anim.in_from_right);
        outToRight = AnimationUtils.loadAnimation(context, R.anim.out_to_right);
        outToLeft = AnimationUtils.loadAnimation(context, R.anim.out_to_left);
        inFromLeft = AnimationUtils.loadAnimation(context, R.anim.in_from_left);
    }

    @Override
    public boolean onFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY){
        float diffX = me2.getX() - me1.getX();
        float diffY = me2.getY() - me1.getY();
        if (Math.abs(diffX) > Math.abs(diffY)) {
            if (Math.abs(diffX) > 10) {
                if(diffX<0){
                    Log.v("DAVASAM", "Swipe Right to Left");
                    if(backLayout.getVisibility()==View.GONE){
                        frontLayout.startAnimation(outToLeft);
                        //backLayout.setVisibility(View.VISIBLE);
                        backLayout.startAnimation(inFromRight);
                        //frontLayout.setVisibility(View.GONE);
                    }
                }else{
                    Log.v("DAVASAM", "Swipe Left to Right");
                    if(backLayout.getVisibility()!=View.GONE){
                        backLayout.startAnimation(outToRight);
                        backLayout.setVisibility(View.GONE);
                        frontLayout.setVisibility(View.VISIBLE);
                        frontLayout.startAnimation(inFromLeft);
                    }
                }
            }
        }

        return true;
    }
}

