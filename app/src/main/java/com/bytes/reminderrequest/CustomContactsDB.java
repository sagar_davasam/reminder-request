package com.bytes.reminderrequest;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by sagar.davasam on 12/27/15.
 */
public class CustomContactsDB {

    private SQLiteDatabase sqLiteDatabase;

    private CustomContactsDBHelper customContactsDBHelper;

    private static final String DATABASE_NAME = "custom_contacts_db";

    private static final int DATABASE_VERSION = 1;

    private static final int READABLE_DB = 0;

    private static final int WRITABLE_DB = 1;

    private static final String TAG = "CustomContactsDB";

    public static final String TABLE_CUSTOM_CONTACTS = "custom_contacts_table";

    public static final String COLUMN_ID = "_id";

    public static final String COLUMN_CONTACT_NAME = SearchManager.SUGGEST_COLUMN_TEXT_1;

    public static final String COLUMN_CONTACT_NUMBER = SearchManager.SUGGEST_COLUMN_TEXT_2;

    public static final String COLUMN_INTENT_ACTION = SearchManager.SUGGEST_COLUMN_INTENT_ACTION;

    public static final HashMap<String, String> COLUMN_MAP = buildColumnMap();

    //foreign key
    public static final String COLUMN_CONTACT_ROW_ID = "contact_row_id";

    private Context mContext;

    public CustomContactsDB(final Context context) {
        mContext = context;
        customContactsDBHelper = new CustomContactsDBHelper(context);
    }

    private static HashMap<String, String> buildColumnMap() {
        HashMap<String, String> columnMap = new HashMap<>();
        columnMap.put(COLUMN_CONTACT_NAME, COLUMN_CONTACT_NAME);
        columnMap.put(COLUMN_CONTACT_NUMBER, COLUMN_CONTACT_NUMBER);
        columnMap.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID, "rowid AS " +
                SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);
        columnMap.put(BaseColumns._ID, "rowid AS " +
                BaseColumns._ID);
        return columnMap;
    }

    public Cursor getWordMatch(String query, String[] columns) {

        String selection = COLUMN_CONTACT_NAME + " MATCH ?";
        String[] selectionArgs = new String[]{query + "*"};

        return query(selection, selectionArgs, columns);
    }

    public Cursor query(String selection, String[] selectionArgs, String[] columns) {

        SQLiteQueryBuilder sqLiteQueryBuilder = new SQLiteQueryBuilder();
        sqLiteQueryBuilder.setTables(TABLE_CUSTOM_CONTACTS);
        sqLiteQueryBuilder.setProjectionMap(COLUMN_MAP);

        return sqLiteQueryBuilder.query(customContactsDBHelper.getReadableDatabase(), columns, selection, selectionArgs, null, null, null);
    }

    //Called from Main Activity (menu item) to sync latest contacts with search suggestions
    public void RefreshContactsDatabase() {
        sqLiteDatabase = customContactsDBHelper.getWritableDatabase();
        sqLiteDatabase.execSQL(customContactsDBHelper.DATABASE_DELETE);
        customContactsDBHelper.onCreate(sqLiteDatabase);
    }


    static class CustomContactsDBHelper extends SQLiteOpenHelper {

        private Context mContext;

        private static final String DATABASE_CREATE = "CREATE VIRTUAL TABLE IF NOT EXISTS " + TABLE_CUSTOM_CONTACTS + " USING fts3 ("
                //+ COLUMN_ID + " integer primary key autoincrement, "
                + COLUMN_CONTACT_NAME + ", "
                + COLUMN_CONTACT_NUMBER + ", "
                + COLUMN_INTENT_ACTION + ");";

        private static final String DATABASE_DELETE = "DROP TABLE IF EXISTS " + TABLE_CUSTOM_CONTACTS;

        public CustomContactsDBHelper(Context context) {

            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            mContext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(DATABASE_CREATE);
            insertData(sqLiteDatabase);
        }

        public void insertData(SQLiteDatabase sqLiteDatabase) {
            try {
                Cursor contactsCursor = mContext.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                contactsCursor.moveToFirst();
                ContentValues contentValues = new ContentValues();

                while (contactsCursor.moveToNext()) {

                    String displayName = contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String phoneNumber = contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    if (displayName.length() > 0 && phoneNumber.length() > 0) {

                        contentValues.put(COLUMN_CONTACT_NAME, displayName);
                        contentValues.put(COLUMN_CONTACT_NUMBER, phoneNumber);
                        sqLiteDatabase.insert(TABLE_CUSTOM_CONTACTS, null, contentValues);
                    }

                }

                contactsCursor.close();
            } catch (Exception ex) {
                Log.e(TAG, "Exception while inserting data in contacts db", ex);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
            sqLiteDatabase.execSQL(DATABASE_DELETE);
            onCreate(sqLiteDatabase);

        }

    }


}
