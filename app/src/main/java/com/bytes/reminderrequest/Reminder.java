package com.bytes.reminderrequest;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sagar.davasam on 11/11/15.
 */
public class Reminder implements Parcelable {

    private String reminderDate;
    private String reminderTime;
    private String reminderMessage;
    private String reminderContact;
    private String recordDate;
    private String recordTime;
    private String reminderTimeStamp;
    private String recordTimeStamp;
    private int bRead;
    private int bReceived;
    private int reminderType;
    private static final String TAG = "Reminder Class";

    protected Reminder(Parcel in) {
        reminderDate = in.readString();
        reminderTime = in.readString();
        reminderMessage = in.readString();
        reminderContact = in.readString();
        recordDate = in.readString();
        recordTime = in.readString();
        reminderTimeStamp = in.readString();
        recordTimeStamp = in.readString();
        bRead = in.readInt();
        bReceived = in.readInt();
        reminderType = in.readInt();
    }

    public static final Creator<Reminder> CREATOR = new Creator<Reminder>() {
        @Override
        public Reminder createFromParcel(Parcel in) {
            return new Reminder(in);
        }

        @Override
        public Reminder[] newArray(int size) {
            return new Reminder[size];
        }
    };

    public String getReminderDate() {
        return this.reminderDate;
    }

    public String getReminderTime() {
        return this.reminderTime;
    }

    public String getReminderTimeStamp() {
        return this.reminderTimeStamp;
    }

    public String getReminderMessage() {
        return this.reminderMessage;
    }

    public String getReminderContact() {
        return this.reminderContact;
    }

    public String getRecordTimeStamp() {
        return this.recordTimeStamp;
    }

    public int getbRead() {
        return bRead;
    }

    public int getbReceived() {
        return bReceived;
    }

    public int getReminderType() {
        return reminderType;
    }

    private Reminder(String reminderMessage, String reminderContact, String reminderTimeStamp, String reminderDate, String reminderTime, String recordTimeStamp, int bRead, int bReceived, int reminderType) {
        this.reminderMessage = reminderMessage;
        this.reminderContact = reminderContact;
        if (reminderTimeStamp == null)
            this.reminderTimeStamp = reminderDate + " " + reminderTime;
        else
            this.reminderTimeStamp = reminderTimeStamp;
        this.reminderDate = reminderDate;
        this.reminderTime = reminderTime;
        this.recordTimeStamp = recordTimeStamp;
        this.bRead = bRead;
        this.bReceived = bReceived;
        this.reminderType = reminderType;
    }

    @Override
    public String toString() {

        return reminderMessage + " " + reminderContact + " " + reminderTimeStamp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(reminderDate);
        dest.writeString(reminderTime);
        dest.writeString(reminderMessage);
        dest.writeString(reminderContact);
        dest.writeString(recordDate);
        dest.writeString(recordTime);
        dest.writeString(reminderTimeStamp);
        dest.writeString(recordTimeStamp);
        dest.writeInt(bRead);
        dest.writeInt(bReceived);
        dest.writeInt(reminderType);
    }

    public static class ReminderBuilder implements Parcelable {

        private String reminderDate;
        private String reminderTime;
        private String reminderMessage;
        private String reminderContact;
        private String recordDate;
        private String recordTime;
        private String reminderTimeStamp;
        private String recordTimeStamp;
        private int bRead;
        private int bReceived;
        private int reminderType;

        public ReminderBuilder() {
            //TODO: set all required defaults here

            this.reminderType = ReminderUtils.ReminderTypes.DEFAULT;
            //this.reminderDate = (new SimpleDateFormat(ReminderUtils.DATE_FORMAT)).format(new Date());
            //this.reminderTime = (new SimpleDateFormat(ReminderUtils.TIME_FORMAT)).format(new Date());
            //this.reminderTimeStamp = this.reminderDate + " " + this.reminderTime;
        }

        protected ReminderBuilder(Parcel in) {
            reminderDate = in.readString();
            reminderTime = in.readString();
            reminderMessage = in.readString();
            reminderContact = in.readString();
            recordDate = in.readString();
            recordTime = in.readString();
            reminderTimeStamp = in.readString();
            recordTimeStamp = in.readString();
            bRead = in.readInt();
            bReceived = in.readInt();
            reminderType = in.readInt();
        }

        public static final Creator<ReminderBuilder> CREATOR = new Creator<ReminderBuilder>() {
            @Override
            public ReminderBuilder createFromParcel(Parcel in) {
                return new ReminderBuilder(in);
            }

            @Override
            public ReminderBuilder[] newArray(int size) {
                return new ReminderBuilder[size];
            }
        };

        public ReminderBuilder setReminderTimeStamp(String reminderDate, String reminderTime) {

            if (reminderDate != null && reminderDate.length() > 0) {
                this.reminderDate = reminderDate;
            }
            if (reminderTime != null && reminderTime.length() > 0) {
                this.reminderTime = reminderTime;
            }

            this.reminderTimeStamp = this.reminderDate + " " + this.reminderTime;

            return this;
        }

        public ReminderBuilder setReminderDate(String reminderDate) {

            //this.setReminderTimeStamp(reminderDate, this.reminderTime);
            if (reminderDate != null && reminderDate.length() > 0) {
                this.reminderDate = reminderDate;
            }
            return this;

        }

        public ReminderBuilder setReminderTime(String reminderTime) {

            //this.setReminderTimeStamp(this.reminderDate, reminderTime);
            if (reminderTime != null && reminderTime.length() > 0) {
                this.reminderTime = reminderTime;
            }
            return this;
        }

        public ReminderBuilder setReminderTimeStamp(String reminderTimeStamp) {

            try {
                String[] reminderTimeStampArray = reminderTimeStamp.split(" ");

                if (reminderTimeStampArray.length == 2) {
                    this.setReminderTimeStamp(reminderTimeStampArray[0], reminderTimeStampArray[1]);
                } else {
                    this.setReminderTimeStamp("", "");
                }

            } catch (Exception ex) {
                Log.e(TAG, "Exception in setReminderTimeStamp(String reminderTimeStamp) method", ex);
            }

            return this;
        }

        public ReminderBuilder setReminderMessage(String reminderMessage) {
            this.reminderMessage = reminderMessage;
            return this;
        }

        public ReminderBuilder setReminderContact(String reminderContact) {
            this.reminderContact = reminderContact;
            return this;
        }

        public ReminderBuilder setRecordTimeStamp() {

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ReminderUtils.TIMESTAMP_FORMAT);
            this.recordTimeStamp = simpleDateFormat.format(new Date());
            return this;
        }

        public ReminderBuilder setbRead(int bRead) {
            this.bRead = bRead;
            return this;
        }

        public ReminderBuilder setbReceived(int bReceived) {
            this.bReceived = bReceived;
            return this;
        }

        public ReminderBuilder setReminderType(int reminderType) {
            this.reminderType = reminderType;
            return this;
        }

        public Reminder createReminder() {
            return new Reminder(reminderMessage, reminderContact, reminderTimeStamp, reminderDate, reminderTime, recordTimeStamp, bRead, bReceived, reminderType);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(reminderDate);
            dest.writeString(reminderTime);
            dest.writeString(reminderMessage);
            dest.writeString(reminderContact);
            dest.writeString(recordDate);
            dest.writeString(recordTime);
            dest.writeString(reminderTimeStamp);
            dest.writeString(recordTimeStamp);
            dest.writeInt(bRead);
            dest.writeInt(bReceived);
            dest.writeInt(reminderType);
        }
    }

}
