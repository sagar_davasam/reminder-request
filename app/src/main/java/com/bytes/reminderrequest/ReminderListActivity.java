package com.bytes.reminderrequest;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class ReminderListActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {//implements LoaderManager.LoaderCallbacks<Cursor> {

    final Context context = this;
    private ViewPager viewPager;
    private FloatingActionButton fab;
    private TabLayout tabLayout;

    private static final int REQUEST_ALL_PERMISSIONS = 1989;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolba_reminder_list_activity);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.compose_fab);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        fab.setOnClickListener(composeFabOnClickeListener);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        //start SMS receiver service
        if (hasAllPermissions()) {
            startService(new Intent(context, ReminderService.class));
            viewPager = (ViewPager) findViewById(R.id.pager);
            viewPager.setAdapter(new HomeFragmentPagerAdapter(getSupportFragmentManager(), ReminderListActivity.this));
            tabLayout.setupWithViewPager(viewPager);
            viewPager.addOnPageChangeListener(tabLayoutPageChangeListener);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_reminder_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (!hasAllPermissions()) {
            return false;
        }
        switch (id) {
            case R.id.action_settings:
                Intent settingsIntent = new Intent(this, ReminderPreferencesActivity.class);
                startActivity(settingsIntent);
                return true;
            case R.id.action_refresh_contacts:
                new RefreshContactsAsyncTask(context, findViewById(R.id.activity_reminder_list_id)).execute();
                return true;
            case R.id.action_show_sent_items:
                Intent sentItemsIntent = new Intent(this, SentItemsActivity.class);
                startActivity(sentItemsIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean hasAllPermissions() {

        if (Build.VERSION.SDK_INT < 23) {
            return true;
        }
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();

        if (!addPermission(permissionsList, Manifest.permission.WRITE_CONTACTS))
            permissionsNeeded.add("Write Contacts");
        if (!addPermission(permissionsList, Manifest.permission.READ_CONTACTS))
            permissionsNeeded.add("Read Contacts");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write External Storage");
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            permissionsNeeded.add("Read External Storage");
        }
        if (!addPermission(permissionsList, Manifest.permission.SEND_SMS))
            permissionsNeeded.add("Send SMS");
        if (!addPermission(permissionsList, Manifest.permission.READ_SMS)) {
            permissionsNeeded.add("Read SMS");
        }
        if (!addPermission(permissionsList, Manifest.permission.RECEIVE_SMS)) {
            permissionsNeeded.add("Receive SMS");
        }

        if (!addPermission(permissionsList, Manifest.permission.VIBRATE)) {
            permissionsNeeded.add("Vibrate");
        }

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {

                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++) {
                    message = message + ", " + permissionsNeeded.get(i);
                }
                showAlertDialog(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(ReminderListActivity.this, permissionsList.toArray(new String[permissionsList.size()]), REQUEST_ALL_PERMISSIONS);
                    }
                });

                return false;
            }
            ActivityCompat.requestPermissions(ReminderListActivity.this, permissionsList.toArray(new String[permissionsList.size()]), REQUEST_ALL_PERMISSIONS);
            return false;
        }

        return true;
    }

    private void showAlertDialog(String message, DialogInterface.OnClickListener dialogClickListener) {
        new AlertDialog.Builder(ReminderListActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", dialogClickListener)
                .setNegativeButton("CANCEL", null)
                .create()
                .show();
    }

    private boolean addPermission(List<String> permissionList, String permission) {
        if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(permission);
            if (!ActivityCompat.shouldShowRequestPermissionRationale(ReminderListActivity.this, permission)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(context, "Some Permission is Denied", Toast.LENGTH_LONG).show();
                break;
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    View.OnClickListener composeFabOnClickeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (hasAllPermissions()) {
                Intent intent = new Intent(context, SendReminder.class);
                startActivity(intent);
            }
        }
    };

    ViewPager.OnPageChangeListener tabLayoutPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            android.support.v4.app.Fragment currentFragment = ((HomeFragmentPagerAdapter) viewPager.getAdapter()).getFragment(position);
            if (currentFragment == null) {
                return;
            }
            currentFragment.onResume();

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

}
