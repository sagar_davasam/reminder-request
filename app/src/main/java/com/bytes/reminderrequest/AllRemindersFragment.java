package com.bytes.reminderrequest;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AllRemindersFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AllRemindersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AllRemindersFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, View.OnFocusChangeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private int mTab;

    private CustomAdapter mAdapter;

    private ReminderRequestDB reminderRequestDB;

    private Cursor mCursor;

    private ListView mListView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param position Parameter 1.
     * @return A new instance of fragment AllRemindersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AllRemindersFragment newInstance(int position) {
        AllRemindersFragment fragment = new AllRemindersFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, position);
        fragment.setArguments(args);
        return fragment;
    }

    public AllRemindersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTab = getArguments().getInt(ARG_PARAM1);
        reminderRequestDB = new ReminderRequestDB(getActivity());
        mAdapter = new CustomAdapter(getActivity(), mCursor, 0);

        mCursor = reminderRequestDB.getAllRecords();

        if (mCursor.getCount() == 0) {
            Toast.makeText(getActivity(), "NO REMINDERS FOUND", Toast.LENGTH_LONG);
        }

        getLoaderManager().initLoader(0x02, null, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_item_future, container, false);

        // Set the adapter
        mListView = (ListView) view.findViewById(R.id.reminder_list_fragment);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(new ListItemClickListener(getActivity(), mAdapter, mTab, this));

        mListView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Cursor allRemindersCursor = reminderRequestDB.getAllRecords();
        if (allRemindersCursor.getCount() == 0) {
            Snackbar.make(getView(), "No reminders", Snackbar.LENGTH_LONG).show();
        }
        mAdapter.changeCursor(allRemindersCursor);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListView.setAdapter(null);
    }

    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new android.support.v4.content.Loader<Cursor>(getActivity());
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(reminderRequestDB.getAllRecords());
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {

        mAdapter.changeCursor(reminderRequestDB.getAllRecords());

    }

   /* @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
