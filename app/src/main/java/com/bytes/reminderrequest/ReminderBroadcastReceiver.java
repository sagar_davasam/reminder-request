package com.bytes.reminderrequest;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

public class ReminderBroadcastReceiver extends BroadcastReceiver {

    private int REMINDER_BROADCAST_RECEIVER_NOTIFICATION_ID = 100;

    @Override
    public void onReceive(Context context, Intent intent) {

        Reminder reminder = intent.getParcelableExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT);
        notifyUser(reminder, context);

    }

    private void notifyUser(Reminder reminder, Context context) {

        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification;

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String notificationSound = sharedPreferences.getString(ReminderPreferencesActivity.REMINDER_RINGTONE_PREFERENCE, "");

        Intent intent = new Intent(context, ReminderDialogActivity.class);
        intent.putExtra(ReminderUtils.REMINDER_PARCELABLE_OBJECT, reminder);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setTicker("Reminder").setWhen(System.currentTimeMillis())
                                    .setSmallIcon(android.R.drawable.ic_popup_reminder)
                                    .setLights(Color.parseColor("#FF4000"),50,500)
                                    .setAutoCancel(true)
                .setContentIntent(PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT))
                .setContentText(reminder.getReminderMessage())
                .setAutoCancel(true)
                .setPriority(1)
                .setContentTitle("Reminder");

        if (notificationSound.length() > 0) {
            builder.setSound(Uri.parse(notificationSound));
        } else {
            builder.setDefaults(Notification.DEFAULT_SOUND);
        }


        boolean bVibrate = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(ReminderPreferencesActivity.REMINDER_VIBRATE_PREFERENCE, false);

        if (bVibrate) {
            builder.setVibrate(new long[]{500, 500, 500, 1500});
        }

        notification = builder.build();
        notificationManager.notify(REMINDER_BROADCAST_RECEIVER_NOTIFICATION_ID, notification);

    }
}
