package com.bytes.reminderrequest;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class FutureRemindersFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PAGE = "ARG_PAGE";

    private OnFragmentInteractionListener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private ListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private CustomAdapter mAdapter;

    private ReminderRequestDB reminderRequestDB;

    private Cursor mCursor;

    private int mTab;

    public static FutureRemindersFragment newInstance(int position) {
        FutureRemindersFragment fragment = new FutureRemindersFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, position);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public FutureRemindersFragment() {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        reminderRequestDB = new ReminderRequestDB(getActivity());
        mAdapter = new CustomAdapter(getActivity(), mCursor, 0);
        mTab = getArguments().getInt(ARG_PAGE);

        mCursor = reminderRequestDB.getFutureRecords();

        if (mCursor == null) {
            Toast.makeText(getActivity(), "NO REMINDERS FOUND", Toast.LENGTH_LONG);
        }

        getLoaderManager().initLoader(0x01, null, this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_future, container, false);

        // Set the adapter
        mListView = (ListView) view.findViewById(R.id.reminder_list_fragment);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(new ListItemClickListener(getActivity(), mAdapter, mTab, this));

        mListView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onResume() {
        super.onResume();

        Cursor futureRecordsCursor = reminderRequestDB.getFutureRecords();

        if (futureRecordsCursor.getCount() == 0) {
            Snackbar.make(getView(), "No upcoming reminders", Snackbar.LENGTH_LONG).show();
        }
        mTab = getArguments().getInt(ARG_PAGE);
        mAdapter.swapCursor(futureRecordsCursor);
        //getLoaderManager().restartLoader(0x01, null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mTab = getArguments().getInt(ARG_PAGE);
        mAdapter.swapCursor(mTab == 1 ? reminderRequestDB.getFutureRecords() : reminderRequestDB.getAllRecords());
    }

    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new android.support.v4.content.Loader<Cursor>(getActivity());
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {

        mAdapter.swapCursor(reminderRequestDB.getFutureRecords());

    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
        mAdapter.swapCursor(null);

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String id);
    }

}
