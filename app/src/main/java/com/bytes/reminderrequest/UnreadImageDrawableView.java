package com.bytes.reminderrequest;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by sagar.davasam on 4/17/16.
 */
public class UnreadImageDrawableView extends View {

    private Paint rectPaint;

    private int rectColor;

    public UnreadImageDrawableView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        rectPaint = new Paint();
        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.UnreadImageDrawableView, 0, 0);
        try {
            rectColor = typedArray.getColor(R.styleable.UnreadImageDrawableView_viewColor, 0);

        } finally {
            typedArray.recycle();
        }

    }

    @Override
    public void onDraw(Canvas canvas) {
        rectPaint.setColor(rectColor);
        rectPaint.setStyle(Paint.Style.FILL);
        canvas.drawRect(new Rect(0, 0, getMeasuredWidth(), getMeasuredHeight()), rectPaint);
    }
}
