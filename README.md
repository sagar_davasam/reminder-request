# Reminder Request #

Reminder Request is an Android app using which a user can send reminders to his/her contacts. Upon receiving a reminder, user can directly add this reminder to scheduled date and time. Reminders are sent and received via SMS protocol.

## Features ##
* Compose a Reminder by selecting date, time and message.
* Send the composed reminder to a contact.
* On receiving a reminder, a notification appears.
* This notification opens reminder details. User can add or discard the reminder.
* User can also reschedule the reminder and edit the reminder message before adding it to the device.

Here's the demo video:
https://www.youtube.com/watch?v=99Zk9CsM1jY